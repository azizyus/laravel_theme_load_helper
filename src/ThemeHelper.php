<?php

function themeDir()
{
    $THEME_DIR = "front_themes.".env(config("themeLoadHelperEnvOptions.frontThemeKey"),"debug_theme");
    return $THEME_DIR;
}

function getThemesBaseDir()
{
    return public_path("views/front_themes");
}

function themeConfigFileFullPath()
{
    $configFilePath = "views/front_themes/".env(config("themeLoadHelperEnvOptions.frontThemeKey"),"debug_theme")."/ThemeConfigFile.php";
    return public_path($configFilePath);
}

function themeClassFullPath()
{
    $classPath = "views/front_themes/".env(config("themeLoadHelperEnvOptions.frontThemeKey"),"debug_theme")."/ThemeHelper.php";
    return public_path($classPath);
}

function themeControllerFullPath()
{
    $controllerPath = "views/front_themes/".env(config("themeLoadHelperEnvOptions.frontThemeKey"),"debug_theme")."/ThemeController.php";
    return public_path($controllerPath);
}

function themeServiceProviderFullPath()
{
    $serviceProviderPath = "views/front_themes/".env(config("themeLoadHelperEnvOptions.frontThemeKey"),"debug_theme")."/ThemeServiceProvider.php";
    return public_path($serviceProviderPath);
}

function themeView($viewName) //works and tested
{

    $THEME_DIR = themeDir();
    $path = $THEME_DIR.".views.".$viewName;

    $view = view($path);



    return $view;


}

function themeViewPath($viewName)
{

    $THEME_DIR = themeDir();
    $path = $THEME_DIR.".views.".$viewName;

    return $path;


}

function themeOptionViewPath($viewName)
{

    $themeOptionViewPath = themeDir();
    $path = $themeOptionViewPath.".options.".$viewName;
    return $path;


}

function themeOptionView($viewName)
{
    return view(themeOptionViewPath($viewName));
}

function themeAsset($assetName)
{
    $themeDir = str_replace(".","/",themeDir());

    $themeDir = "views/$themeDir/assets";

    $fullAssetPath = $themeDir."/".$assetName;
    $link = asset($fullAssetPath);

    return $link;

}

function replace($string,$search=".",$seperator=null)
{

    if($seperator!=null)
    {
        $string = str_replace($search,$seperator,$string);
    }

    return $string;

}
function themeTranslationNameSpace()
{
    return "Theme";
}
function themeTrans($name)
{
    return trans(themeTranslationNameSpace()."::$name");
}

function themeTranslationDir()
{

    $THEME_DIR = themeDir();
    $path = $THEME_DIR.".translations";

    $path = public_path("views/$path");
    $path = replace($path,".","/");
    return $path;

}


function getThemeLogo()
{
    return themeAsset("img/logo.png");
}