<?php


namespace LaravelThemeLoader;


class ThemeClassLoader
{


    protected function getThemeClassInstancePath()
    {
        return themeClassFullPath();
    }


    public function getThemeHelperClassInstance()
    {
        $path = $this->getThemeClassInstancePath();
        include_once $path;
        $themeClassInstance = new \ThemeHelper();
        return $themeClassInstance;
    }

    public function getServiceProviderFullNamespace()
    {
        $path = themeServiceProviderFullPath();
        include_once $path;
        return "\ThemeServiceProvider";
    }

    public function getThemeControllerFullPath()
    {
        return themeControllerFullPath();
    }

    public function getThemeControllerInstance()
    {
        $path = themeControllerFullPath();
        include_once $path;
        return new \ThemeController();
    }



}