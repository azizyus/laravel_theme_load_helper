<?php

function panelDir()
{
    return env("PANEL_DIR","panel");
}

function panelView($view)
{
    return view(panelViewPath($view));
}

function panelAsset($asset)
{
    return asset("views/".panelDir()."/"."assets"."/".$asset);
}

function panelViewPath($view)
{
    return panelDir().".views.".$view;
}

