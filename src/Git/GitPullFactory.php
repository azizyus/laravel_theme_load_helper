<?php


namespace LaravelThemeLoader\Git;


class GitPullFactory
{

    public static function make()
    {
        $g = new GitPullHelper();
        $g->setBasePath(getThemesBaseDir());
        return $g;
    }

}