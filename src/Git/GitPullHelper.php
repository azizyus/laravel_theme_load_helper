<?php


namespace LaravelThemeLoader\Git;


use Illuminate\Support\Facades\File;

class GitPullHelper
{

    protected $basePath;
    public function __construct()
    {

    }

    public function setBasePath(String $path)
    {
        $this->basePath = $path;
    }

    public function findAllThemeDirectories()
    {
        return File::directories($this->basePath);
    }


    public function pullAllThemes($excludedThemes = ["debug_theme"])
    {
        foreach ($this->findAllThemeDirectories() as $directory)
        {
            if(!in_array(basename($directory),$excludedThemes)) shell_exec("cd $directory && git pull");
        }
    }






}