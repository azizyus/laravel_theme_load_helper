<?php

namespace LaravelThemeLoader;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
class LaravelThemeLoaderServiceProvider extends ServiceProvider
{


    public function register()
    {

    }

    public function boot()
    {

//        Blade::directive("extendsPanel",function ($param){
//
//
//            $param = panelViewPath($param);
//            $param = str_replace('"','',$param);
//
//            return Blade::compileString("@extends('$param')");
//
//        });


        //couldnt find doc about how @extend works so i ll just namespace'd em

        $this->mergeConfigFrom(__DIR__."/Config.php","themeLoadHelperEnvOptions");

        $panelViewPath = panelViewPath("");
        $panelViewPath = str_replace(".","/",$panelViewPath);
        $viewPath =  public_path('').'/views/'.$panelViewPath;
        $this->loadViewsFrom($viewPath,"Panel");

        $themeViewPath = themeViewPath("");
        $themeViewPath = str_replace(".","/",$themeViewPath);
        $viewPath = public_path('').'/views/'.$themeViewPath;
        $this->loadViewsFrom($viewPath,"Theme");

    }

}
