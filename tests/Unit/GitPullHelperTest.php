<?php

namespace Tests\Unit;

use Orchestra\Testbench\TestCase;
use LaravelThemeLoader\Git\GitPullHelper;

class GitPullHelperTest extends TestCase
{

    protected $gitPullHelper;
    protected $baseDir;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->gitPullHelper = new GitPullHelper();
        $this->baseDir = __DIR__."/public_test_dir";
        $this->gitPullHelper->setBasePath($this->baseDir);
    }

    public function test_directory_read()
    {

       $dirs = $this->gitPullHelper->findAllThemeDirectories();
       if($dirs == ["$this->baseDir/aa","$this->baseDir/bb","$this->baseDir/cc"])
         $this->assertTrue(true);
       else
        $this->assertFalse(true);
    }

}